﻿namespace SignalExport
{
    public enum PduHeaderType
    {
        BCC = 129,
        CC = 130,
        FROM = 137,
        TO = 150,
        MESSAGE_TYPE_SEND_REQ = 128,
        MESSAGE_TYPE_RETRIEVE_CONF = 132
    }

    public enum SmsMessageType
    {
        //TOTAL_MASK = 0xFFFFFFFF

        // Base Types
        BASE_TYPE_MASK = 31,

        INCOMING_AUDIO_CALL_TYPE = 1,
        MISSED_AUDIO_CALL_TYPE = 3,
        JOINED_TYPE = 4,
        UNSUPPORTED_MESSAGE_TYPE = 5,
        INVALID_MESSAGE_TYPE = 6,
        PROFILE_CHANGE_TYPE = 7,
        MISSED_VIDEO_CALL_TYPE = 8,
        GV1_MIGRATION_TYPE = 9,
        INCOMING_VIDEO_CALL_TYPE = 16,
        GROUP_CALL_TYPE = 18,

        BASE_INBOX_TYPE = 32,
        BASE_DRAFT_TYPE = 39,

        // outgoing message types

        BASE_OUTBOX_TYPE = 33,
        BASE_SENDING_TYPE = 34,
        BASE_SENT_TYPE = 35,
        BASE_SENT_FAILED_TYPE = 36,
        BASE_PENDING_SECURE_SMS_FALLBACK = 37,
        BASE_PENDING_INSECURE_SMS_FALLBACK = 38,
        OUTGOING_AUDIO_CALL_TYPE = 2,
        OUTGOING_VIDEO_CALL_TYPE = 17,

        // Message attributes

        MESSAGE_ATTRIBUTE_MASK = 224,
        MESSAGE_RATE_LIMITED_BIT = 128,
        MESSAGE_FORCE_SMS_BIT = 64,

        // Key Exchange Information

        KEY_EXCHANGE_MASK = 65280,
        KEY_EXCHANGE_BIT = 32768,
        KEY_EXCHANGE_IDENTITY_VERIFIED_BIT = 16384,
        KEY_EXCHANGE_IDENTITY_DEFAULT_BIT = 8192,
        KEY_EXCHANGE_CORRUPTED_BIT = 4096,
        KEY_EXCHANGE_INVALID_VERSION_BIT = 2048,
        KEY_EXCHANGE_BUNDLE_BIT = 1024,
        KEY_EXCHANGE_IDENTITY_UPDATE_BIT = 512,
        KEY_EXCHANGE_CONTENT_FORMAT = 256,

        // Secure Message Information

        SECURE_MESSAGE_BIT = 8388608,
        END_SESSION_BIT = 4194304,
        PUSH_MESSAGE_BIT = 2097152,

        // Group Message Information

        GROUP_UPDATE_BIT = 65536,
        GROUP_QUIT_BIT = 131072,
        EXPIRATION_TIMER_UPDATE_BIT = 262144,
        GROUP_V2_BIT = 524288,

        // Encrypted Storage Information XXX

        //ENCRYPTION_MASK = 0xFF000000;
        //ENCRYPTION_REMOTE_BIT= 0x20000000;
        //ENCRYPTION_REMOTE_FAILED_BIT = 0x10000000;
        //ENCRYPTION_REMOTE_NO_SESSION_BIT = 0x08000000;
        //ENCRYPTION_REMOTE_DUPLICATE_BIT= 0x04000000;
        //ENCRYPTION_REMOTE_LEGACY_BIT = 0x02000000;
    }
}