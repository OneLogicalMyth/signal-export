﻿using System;
using System.IO;
using System.Linq;
using Microsoft.Data.Sqlite;

namespace SignalExport
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var dir = new DirectoryInfo(Directory.GetCurrentDirectory());
            if (args.Length != 0)
            {
                dir = new DirectoryInfo(args[0]);
            }

            if (dir.Exists)
            {
                var databaseFile = Path.Join(dir.FullName, "signal_backup.db");
                if (File.Exists(databaseFile))
                {
                    var connectionString = new SqliteConnectionStringBuilder()
                    {
                        Mode = SqliteOpenMode.ReadOnly,
                        DataSource = databaseFile
                    }.ToString();

                    var database = new SignalDatabase(connectionString);
                    var messages = database.Messages.GroupBy(g => g.ThreadId);

                    foreach (var thread in messages)
                    {
                        var htmlExport = new HtmlExport(thread);
                        htmlExport.Save(Path.Join(dir.FullName, $"message_thread_{thread.Key}.html"));
                    }
                }
                else
                {
                    Console.WriteLine($"Signal SQLite database not found... Looking for {databaseFile}");
                }
            }
            else
            {
                Console.WriteLine(@"Please run provide a valid directory: SignalExport.exe c:\path\to\signal\backup");
            }
        }
    }
}