﻿using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;

namespace SignalExport
{
    internal class SignalDatabase
    {
        public IEnumerable<Message> Messages { get; }

        public SignalDatabase(string connectionString)
        {
            var messages = new List<Message>();

            using (var connection = new SqliteConnection(connectionString))
            {
                connection.Open();

                var command = connection.CreateCommand();
                command.CommandText =
                @"
                        SELECT
                            _id AS id,
                            thread_id,
                            (SELECT phone FROM recipient WHERE _id = sms.address) AS phone,
                            date,
                            body,
                            NULL AS quote_body,
                            NULL AS attachment,
                            NULL AS attachment_type,
                            Type AS sms_type,
                            NULL AS mms_type
                        FROM sms
                    UNION
                        SELECT
                            _id AS id,
                            thread_id,
                            (SELECT phone FROM recipient WHERE _id = mms.address) AS phone,
                            date,
                            body,
                            quote_body,
                            (SELECT unique_id || '_' || _id FROM part WHERE mid = mms._id) AS attachment,
                            (SELECT ct FROM part WHERE mid = mms._id) AS attachment_type,
                            NULL AS sms_type,
                            m_type AS mms_type
                        FROM mms
                    ORDER BY thread_id, date
                ";

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = reader.GetInt32(0);
                        var thread_id = reader.GetInt32(1);
                        var phone = reader.GetString(2);
                        var date = DateTimeOffset
                            .FromUnixTimeMilliseconds(reader.GetInt64(3))
                            .DateTime.ToLocalTime();

                        var body = String.Empty;
                        if (reader.IsDBNull(4) == false)
                        {
                            body = reader.GetString(4);
                        }

                        var quote_body = String.Empty;
                        if (reader.IsDBNull(5) == false)
                        {
                            quote_body = reader.GetString(5);
                        }

                        var attachment = String.Empty;
                        if (reader.IsDBNull(6) == false)
                        {
                            attachment = reader.GetString(6);
                        }

                        var attachment_type = String.Empty;
                        if (reader.IsDBNull(7) == false)
                        {
                            attachment_type = reader.GetString(7);
                        }

                        long sms_type = 0;
                        var smsTypes = new List<SmsMessageType>();
                        if (reader.IsDBNull(8) == false)
                        {
                            sms_type = reader.GetInt64(8);
                            smsTypes = GetSmsMessageTypes(sms_type);
                        }

                        long mms_type = 0;
                        if (reader.IsDBNull(9) == false)
                        {
                            mms_type = reader.GetInt64(9);
                        }

                        messages.Add(new Message(
                            id,
                            thread_id,
                            phone,
                            date,
                            body,
                            quote_body,
                            attachment,
                            attachment_type,
                            smsTypes,
                            (PduHeaderType)mms_type
                            ));
                    }
                }

                Messages = messages;
            }
        }

        private List<SmsMessageType> GetSmsMessageTypes(Int64 sms_type)
        {
            var messageTypes = new List<SmsMessageType>();

            foreach (int i in Enum.GetValues(typeof(SmsMessageType)))
            {
                var band = sms_type & i;

                if (band != 0)
                {
                    messageTypes.Add((SmsMessageType)i);
                }
            }

            return messageTypes;
        }
    }
}