﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalExport
{
    internal class HtmlTemplate
    {
        public static string GetWebTemplate()
        {
            return @"
                    <!DOCTYPE html>
                    <html lang=""en"">
                    <head>
                        <meta charset=""UTF-8"">
                        <title>Chat Messages</title>
                        <meta name=""viewport"" content=""width=device-width,initial-scale=1"">
                        <link rel=""stylesheet"" href="""">
                        <style>
                        </style>
                        <!-- CSS only -->
                        <link href=""https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css"" rel=""stylesheet"" integrity=""sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x"" crossorigin=""anonymous"">
                    </head>
                    <body>
                        <div class=""container stick-top"">
                            <div class=""row"">
                                <div class=""col"">
                                    <h1 class=""display-1"">Chat Messages</h1>
                                </div>
                            </div>
                        </div>

                        <div class=""container"">
                            <div class=""row"">
                                <div class=""col"">
                                    <ul class=""list-group list-group-flush"">
                                        {0}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </body>
                    </html>
                    ";
        }
    }
}