﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SignalExport
{
    internal class HtmlExport
    {
        private IEnumerable<Message> _Messages { get; }
        private string _WebContent { get; set; }

        public HtmlExport(IEnumerable<Message> messages)
        {
            _Messages = messages;
            GenerateHtml();
        }

        public void Save(string filePath)
        {
            using (StreamWriter outputFile = new StreamWriter(filePath))
            {
                outputFile.WriteLine(_WebContent);
            }
        }

        private void GenerateHtml()
        {
            var page = HtmlTemplate.GetWebTemplate();

            var htmlMessages = new List<string>();
            foreach (var message in _Messages)
            {
                htmlMessages.Add(GenerateCard(message));
            }

            _WebContent = String.Format(page, String.Join(Environment.NewLine, htmlMessages));
        }

        private string GenerateCard(Message message)
        {
            var card = @"
                        <li class=""list-group-item"">
                            <div class=""card text-center w-50 float-{5}"">
                                {2}
                                <div class=""card-body"">
                                <p class=""card-text"">{3}</p>
                                </div>
                                <div class=""card-footer text-muted"">{0} from {1} - {4}</div>
                            </div>
                        </li>
                    ";
            //< div class=""card-header""></div>

            var body = message.Body;

            var direction = "Incoming";
            var floatText = "start";
            if (message.IsOutgoing)
            {
                floatText = "end";
                direction = "Outgoing";
            }

            var header = "";
            if (String.IsNullOrWhiteSpace(message.BodyQuote) == false)
            {
                header = $"<div class=\"card-header fst-italic text-info bg-dark\">{message.BodyQuote}</div>";
            }

            var fileExtension = ContentType.GetExtension(message.AttachmentType);
            if (String.IsNullOrWhiteSpace(message.Attachment) == false && String.IsNullOrWhiteSpace(fileExtension) == false)
            {
                if (message.AttachmentType.StartsWith("image"))
                {
                    header = $"<img src=\"./attachment/{message.Attachment}.{fileExtension}\" class=\"card-img-top img-thumbnail\">";
                }
                else
                {
                    header = "";
                    body = $"<a href=\"./attachment/{message.Attachment}.{fileExtension}\">{message.Attachment}.{fileExtension}</a>";
                }
            }

            return String.Format(card, direction, message.Phone, header, body, message.Date, floatText);
        }
    }
}