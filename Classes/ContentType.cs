﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalExport
{
    internal class ContentType
    {
        private static Dictionary<string, string> ContentTypes = new Dictionary<string, string> {
                { "text/plain","txt" },
                { "image/tiff","tiff" },
                { "application/x-bittorrent","torrent" },
                { "application/x-font-ttf","ttf" },
                { "application/x-cdlink","vcd" },
                { "text/x-vcard","vcf" },
                { "application/xml","xml" },
                { "audio/x-wav","wav" },
                { "audio/x-ms-wma","wma" },
                { "video/x-ms-wmv","wmv" },
                { "application/wordperfect","wpd" },
                { "application/xhtml+xml","xhtml" },
                { "application/vnd.ms-excel","xls" },
                { "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","xlsx" },
                { "text/xml","xml" },
                { "video/3gpp2","3g2" },
                { "video/3gpp","3gp" },
                { "application/illustrator","ai" },
                { "audio/x-aiff","aif" },
                { "application/vnd.android.package-archive","apk" },
                { "video/x-ms-asf","asf" },
                { "video/x-msvideo","avi" },
                { "image/bmp","bmp" },
                { "text/x-csrc","c" },
                { "application/x-x509-ca-cert","cer" },
                { "text/x-c++src","cpp" },
                { "application/x-chrome-extension","crx" },
                { "text/css","css" },
                { "text/csv","csv" },
                { "image/vnd.ms-dds","dds" },
                { "application/x-debian-package","deb" },
                { "application/msword","doc" },
                { "application/vnd.openxmlformats-officedocument.wordprocessingml.document","docx" },
                { "application/xml-dtd","dtd" },
                { "application/dxf","dxf" },
                { "video/x-flv","flv" },
                { "image/gif","gif" },
                { "application/gpx+xml","gpx" },
                { "application/x-gzip","gz" },
                { "text/x-chdr","h" },
                { "application/mac-binhex40","hqx" },
                { "text/html","html" },
                { "image/x-icon","ico" },
                { "text/calendar","ics" },
                { "image/jpeg","jpg" },
                { "application/vnd.google-earth.kml+xml","kml" },
                { "application/vnd.google-earth.kmz","kmz" },
                { "audio/x-mpegurl","m3u" },
                { "audio/mp4","m4a" },
                { "video/x-m4v","m4v" },
                { "audio/midi","mid" },
                { "video/quicktime","mov" },
                { "video/mp4","mp4" },
                { "video/mpeg","mpg" },
                { "application/vnd.oasis.opendocument.text","odt" },
                { "application/vnd.oasis.opendocument.formula-template","otf" },
                { "application/x-iwork-pages-sffpages","pages" },
                { "chemical/x-pdb","pdb" },
                { "application/pdf","pdf" },
                { "application/x-httpd-php","php" },
                { "image/png","png" },
                { "application/vnd.ms-powerpoint","ppt" },
                { "application/vnd.openxmlformats-officedocument.presentationml.presentation","pptx" },
                { "application/pics-rules","prf" },
                { "application/postscript","ps" },
                { "application/photoshop","psd" },
                { "audio/x-pn-realaudio","rm" },
                { "application/x-rpm","rpm" },
                { "application/rtf","rtf" },
                { "application/x-sh","sh" },
                { "image/svg+xml","svg" },
                { "application/x-shockwave-flash","swf" },
                { "application/x-tex","tex" }
            };

        public static string GetExtension(string contentType)
        {
            if (ContentTypes.ContainsKey(contentType))
            {
                return ContentTypes[contentType];
            }

            return String.Empty;
        }
    }
}