﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SignalExport
{
    public record Message(
        int Id,
        int ThreadId,
        string Phone,
        DateTime Date,
        string Body,
        string BodyQuote,
        string Attachment,
        string AttachmentType,
        IEnumerable<SmsMessageType> SmsTypes,
        PduHeaderType MmsType
    )
    {
        public bool IsOutgoing
        {
            get
            {
                if (SmsTypes.Contains(SmsMessageType.BASE_SENT_TYPE))
                {
                    return true;
                }

                if (MmsType == PduHeaderType.MESSAGE_TYPE_SEND_REQ)
                {
                    return true;
                }

                return false;
            }
        }
    }
}