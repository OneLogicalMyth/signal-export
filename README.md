# Signal Export

It may not be pretty but it kind of works...

# Decrypting the Signal Backup

1. In Signal enable backup and not the numeric pin given save for later in pass.txt
2. Copy the Signal backup and pass.txt to C:\signalbackup
3. Launch a docker container to decrypt the backup `docker run -v c:\signalbackup:/backup/ -it --rm rust /bin/bash`
4. When at the root prompt of the container install signal-backup-decode with `gargo install signal-backup-decode`
5. Change dir to backup folder within the docker container `cd /backup`
6. Run the following `signal-backup-decode ./signal-2021-05-04-14-28-39.backup --password-file ./pass.txt`
  * Change the date of the Signal backup to match yours!

# Exporting Messages to HTML

1. Download the latest artifacts from the last succesful build of this repo
2. Launch a cmd or PowerShell prompt
3. Run `SignalExport.exe c:\signalbackup`
